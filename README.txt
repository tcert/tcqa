TCQA - TCERT Qualified Auditors certification 

Overview:
========
TCQA certification is attributed to auditors of Telecom infrastructure based
on their skills. The goal of such certification is to qualify and distinguish
the companies with unique skills and capabilities in Telecom Auditing.
Contrary to many other certification process, no cost is associated with this
certification.

TCERT Status:
============
TCQA has not yet received its TRFC number from TCERT. The TCQA project is in
public testing period.

Process:
=======
To be certified by TCERT, follow this process.
1. Send to TCERT a description of your company, number of employee,
   specificities. TCERT email address is listed on: http://tcert.org/
2. Certification exam time and date will be sent to you by TCERT
   representative. More details may be asked about your company, services and
   capabilities.
3. Certification exam will happen over the Internet by both interview,
   questionnaire and technical tests
4. Certification results will be communicated at most 2 weeks to the company.
5. Certification will be published on TCERT TCQA repository.

Technology categories for certification:
=======================================
GSM PS
GSM CS
UMTS PS
UMTS CS
LTE
SMS
SS7
Diameter
ISDN
Analogue telecom systems
Fraud Management
Network Element Security (configuration)
Network Element Vulnerability Research

F.A.Q:
=====
Q: Can individuals be certified?
A: Yes if they are independent freelance workers.

Q: Does this apply to Fraud Management related companies?
A: Yes as of September 2013, Fraud Management related companies can be
certified.

Q: How long does one certification stand?
A: 1 year, that is 365 days.

Q: Does the date of certification resets with each certification?
A: No, the original certification date stays.

Q: When did TCQA start?
A: The prototyping of this initiative started in June 2013.

Q: Are certification failures communicated?
A: Never.

Q: After a certification failures, is there a wait period?
A: Yes, you need to wait 2 month before being able to pass the certification
again.

Q: Can a company ask for certification but ask not to be published in the
   certified companies?
A: No.

Q: Can a company ask for removal from published certified companies list?
A: Yes, but the certification certificate will be withdrawn from such company.
If TCERT is contacted by one entity seeking to know if such company is
certified, TCERT will answer that the company asked for removal of
certification.

Q: Who are the Certifying auditors who certify companies?
A: These are experts from the Research Group of TCERT. See TCERT website.
